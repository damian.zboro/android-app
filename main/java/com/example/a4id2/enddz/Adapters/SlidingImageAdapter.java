package com.example.a4id2.enddz.Adapters;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a4id2.enddz.Helpers.ImageDataJSON;
import com.example.a4id2.enddz.Helpers.LoadImageTask;
import com.example.a4id2.enddz.R;

import java.util.ArrayList;


public class SlidingImageAdapter extends PagerAdapter {


    private ArrayList<ImageDataJSON> imageModelArrayList;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImageAdapter(Context context, ArrayList<ImageDataJSON> imageModelArrayList) {
        this.context = context;
        this.imageModelArrayList = imageModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageModelArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;

        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
        new LoadImageTask(imageView).execute("http://51.255.196.155/android/img/" + imageModelArrayList.get(position).getImageName());
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String[] tmp = imageModelArrayList.get(position).getImageName().split("_");
                String img = tmp[0] + "_" + tmp[1] + ".jpeg";
                Intent intent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://51.255.196.155/android/img/" + img)
                );
                context.startActivity(intent);
                return true;
            }
        });

        TextView tvSave = (TextView) imageLayout.findViewById(R.id.imgSave);
        tvSave.setText(imageModelArrayList.get(position).getImageSaveTime());
        TextView tvSize = (TextView) imageLayout.findViewById(R.id.imgSize);
        tvSize.setText(imageModelArrayList.get(position).getImageSize() + " bytes");

        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}