package com.example.a4id2.enddz.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.a4id2.enddz.R;

import java.util.ArrayList;

/**
 * Created by 4id2 on 2017-11-15.
 */
public class MyDrawerAdapter extends ArrayAdapter{

    private ArrayList<String> _list;
    private Context _context;
    private int _resource;

    public MyDrawerAdapter(Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        this._list = objects;
        this._context = context;
        this._resource = resource;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(_resource, null);

        ImageView iv = (ImageView) convertView.findViewById(R.id.fontImg);
        TextView tv = (TextView) convertView.findViewById(R.id.fontText);

        switch (position){
            case 0:
                iv.setImageResource(R.drawable.font);
                break;
            case 1:
                iv.setImageResource(R.drawable.upload);
                break;
            case 2:
                iv.setImageResource(R.drawable.share);
                break;
            default:
                iv.setImageResource(R.drawable.folder);
        }

        tv.setText(_list.get(position));
        return convertView;
    }
}
