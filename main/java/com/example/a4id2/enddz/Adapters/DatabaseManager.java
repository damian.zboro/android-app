package com.example.a4id2.enddz.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.a4id2.enddz.Helpers.Note;

import java.util.ArrayList;

/**
 * Created by 4id2 on 2017-10-04.
 */
public class DatabaseManager extends SQLiteOpenHelper {
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE note (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'title' TEXT, 'content' TEXT, 'color' TEXT, 'path' TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public DatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void delete(String ID){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "note" + " where _id='" + ID + "'");
    }

    public void update(String ID, String title, String content, String color){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("content", content);
        //db.update("note", contentValues, "_id=? " + ID, new String[]{ID});

        if (color!=null){
            //contentValues.put("color", color);
            db.execSQL("UPDATE note SET title='"+title+"', content='"+content+"', color='"+color+"' WHERE _id=" + ID);
        }else{
            db.execSQL("UPDATE note SET title='"+title+"', content='"+content+"' WHERE _id=" + ID);
        }

        db.close();
    }

    public boolean insert(String title, String content, String color, String path){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("content", content);
        contentValues.put("color", color);
        contentValues.put("path", path);

        db.insertOrThrow("note", null, contentValues); // gdy insert się nie powiedzie, będzie błąd
        db.close();
        return true;
    }

    public ArrayList<Note> getAll(){

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Note> notes= new ArrayList<>();
        Cursor result = db.rawQuery("SELECT * FROM note" , null);
        while(result.moveToNext()){
            notes.add( new Note(
                    result.getString(result.getColumnIndex("_id")),
                    result.getString(result.getColumnIndex("title")),
                    result.getString(result.getColumnIndex("content")),
                    result.getString(result.getColumnIndex("color")),
                    result.getString(result.getColumnIndex("path"))
            ));

        }
        return notes;
    }
}
