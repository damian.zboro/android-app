package com.example.a4id2.enddz.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a4id2.enddz.Helpers.Note;
import com.example.a4id2.enddz.R;

import java.util.ArrayList;

public class MyArrayAdapter extends ArrayAdapter {


    private ArrayList <Note> _list;
    private Context _context;
    private int _resource;

    public MyArrayAdapter(Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        this._list = objects;
        this._context = context;
        this._resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("WW", String.valueOf(position));

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(_resource, null);

        TextView tv1 = (TextView) convertView.findViewById(R.id.noteID);
        tv1.setText(_list.get(position).get_id());


        TextView tv2 = (TextView) convertView.findViewById(R.id.noteTitle);
        tv2.setText(_list.get(position).getnTitle());

        Integer intColor = Integer.parseInt(_list.get(position).getnColor());
        tv2.setTextColor(intColor);

        TextView tv3 = (TextView) convertView.findViewById(R.id.noteContent);
        tv3.setText(_list.get(position).getnContent());


        ImageView iv1 = (ImageView) convertView.findViewById(R.id.notePen);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ZZ", "penLogo");
            }
        });

        return convertView;
    }

}