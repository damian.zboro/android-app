package com.example.a4id2.enddz.Activities;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.a4id2.enddz.Helpers.Kolo;
import com.example.a4id2.enddz.Helpers.Miniatura;
import com.example.a4id2.enddz.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.a4id2.enddz.Helpers.CameraPreview;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout _frameLayout;
    private Camera.Parameters camParams;
    private Camera.Parameters params;

    private ImageButton makePic;
    //private ImageButton savePic;

    private byte[] fdata = null;

    private boolean touch = false;
    private LinearLayout topCam;
    private LinearLayout botCam;
    private OrientationEventListener orientationEventListener;

    private ImageButton camWhi;
    private ImageButton camLig;
    private ImageButton camCol;
    private ImageButton camSiz;

    private int tmpX = 1;
    private float tmpK = 0;
    private float tmpAll = 0;


    private ArrayList<byte[]> imgList = new ArrayList<byte[]>() ;
    private ArrayList<Miniatura> minList = new ArrayList<>();
    private int imgIndex = 0;
    private float x1,x2;

    private String[] spinnerArray;
    private Spinner spinner;

    private LinearLayout Lay;
    private ImageView imgLay;

    private Display display;
    private Point size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        getSupportActionBar().hide();
        _frameLayout = (FrameLayout) findViewById(R.id.cameraLayout);

        Bundle bundle = getIntent().getExtras();
        String value = bundle.getString("key");

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        makePic = (ImageButton) findViewById(R.id.makePic);
        //savePic = (ImageButton) findViewById(R.id.savePic);

        camWhi = (ImageButton) findViewById(R.id.camWhi);
        camLig = (ImageButton) findViewById(R.id.camLig);
        camCol = (ImageButton) findViewById(R.id.camCol);
        camSiz = (ImageButton) findViewById(R.id.camSiz);

        //Camera
        initCamera();
        initPreview();
        //
        camParams = camera.getParameters();

        Lay = (LinearLayout) findViewById(R.id.miniLay);
        imgLay = (ImageView) findViewById(R.id.imgLay);

        spinnerArray = new String[]{"Wybierz opcje", "Zapisz ostatnie","Zapisz wszystkie","Usuń wszystkie"};
        spinner = (Spinner) findViewById(R.id.spinnerCam);

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                CameraActivity.this,
                R.layout.spinner_row,
                R.id.rowSpinner,
                spinnerArray );
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("XXX", spinnerArray[i]);

                File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                File dir = new File(pic, "Album");
                final File[] files = dir.listFiles();

                String[] array = new String[files.length];
                for(int q=0; q<files.length; q++){
                    array[q] = files[q].getName();
                }

                if(imgList.size() != 0 ){
                    switch (spinnerArray[i]){
                        case "Zapisz ostatnie":

                            AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                            alert.setTitle("Wybierz lokalizację zdjęcia!");

                            alert.setItems(array, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Bitmap imgData = BitmapFactory.decodeByteArray(imgList.get(imgList.size()-1), 0, imgList.get(imgList.size()-1).length);
                                    Matrix matrix = new Matrix();
                                    matrix.postRotate(90);
                                    final Bitmap rotatedBitmap = Bitmap.createBitmap(imgData, 0, 0, imgData.getWidth(), imgData.getHeight(), matrix, true);

                                    SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                    String d = dFormat.format(new Date());
                                    FileOutputStream fs = null;

                                    try {
                                        fs = new FileOutputStream(files[which].getPath() + "/" + d);
                                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fs);
                                    } catch (FileNotFoundException e) {

                                    }
                                    try {
                                        fs.close();
                                    } catch (IOException e) {

                                    }

                                }
                            });
                            alert.show();
                            break;
                        case "Zapisz wszystkie":
                            AlertDialog.Builder alertAll = new AlertDialog.Builder(CameraActivity.this);
                            alertAll.setTitle("Wybierz lokalizację zdjęć!");

                            alertAll.setItems(array, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    for(int w=0; w < imgList.size(); w++){
                                        Bitmap imgData = BitmapFactory.decodeByteArray(imgList.get(w), 0, imgList.get(w).length);
                                        Matrix matrix = new Matrix();
                                        matrix.postRotate(90);
                                        final Bitmap rotatedBitmap = Bitmap.createBitmap(imgData, 0, 0, imgData.getWidth(), imgData.getHeight(), matrix, true);

                                        SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                        String d = dFormat.format(new Date()) + w;
                                        FileOutputStream fs = null;

                                        try {
                                            fs = new FileOutputStream(files[which].getPath() + "/" + d);
                                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fs);
                                        } catch (FileNotFoundException e) {

                                        }
                                        try {
                                            fs.close();
                                        } catch (IOException e) {

                                        }
                                    }

                                }
                            });
                            alertAll.show();
                            break;
                        case "Usuń wszystkie":
                            for (int j=_frameLayout.getChildCount()-1; j>=2; j--){
                                _frameLayout.removeViewAt(j);
                            }
                            imgList.clear();
                            imgIndex = 0;
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("XXX", "NULL");
            }
        });



        orientationEventListener = new OrientationEventListener(getApplicationContext()) {
            @Override
            public void onOrientationChanged(int i) {
                //Log.d("WW", String.valueOf(i));

                if (i>80 && i<100 && camLig.getRotation() == 0.0){
                    ObjectAnimator.ofFloat(camWhi, View.ROTATION, 0, -90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camLig, View.ROTATION, 0, -90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camCol, View.ROTATION, 0, -90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camSiz, View.ROTATION, 0, -90).setDuration(300).start();

                    for (int j=2; j<_frameLayout.getChildCount(); j++){
                        ObjectAnimator.ofFloat(_frameLayout.getChildAt(j), View.ROTATION, 0, -90).setDuration(300).start();
                    }
                }else if((i<10 || i>350) && (camLig.getRotation() == 90.0 || camLig.getRotation() == -90.0)){
                    ObjectAnimator.ofFloat(camWhi, View.ROTATION, -90, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(camWhi, View.ROTATION, 90, 0).setDuration(300).start();

                    ObjectAnimator.ofFloat(camLig, View.ROTATION, -90, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(camLig, View.ROTATION, 90, 0).setDuration(300).start();

                    ObjectAnimator.ofFloat(camCol, View.ROTATION, -90, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(camCol, View.ROTATION, 90, 0).setDuration(300).start();

                    ObjectAnimator.ofFloat(camSiz, View.ROTATION, -90, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(camSiz, View.ROTATION, 90, 0).setDuration(300).start();

                    for (int j=2; j<_frameLayout.getChildCount(); j++){
                        ObjectAnimator.ofFloat(_frameLayout.getChildAt(j), View.ROTATION, -90, 0).setDuration(300).start();
                        ObjectAnimator.ofFloat(_frameLayout.getChildAt(j), View.ROTATION, 90, 0).setDuration(300).start();
                    }

                }else if(i>260 && i<280 && camLig.getRotation() == 0.0){
                    ObjectAnimator.ofFloat(camWhi, View.ROTATION, 0, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camLig, View.ROTATION, 0, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camCol, View.ROTATION, 0, 90).setDuration(300).start();
                    ObjectAnimator.ofFloat(camSiz, View.ROTATION, 0, 90).setDuration(300).start();

                    for (int j=2; j<_frameLayout.getChildCount(); j++){
                        ObjectAnimator.ofFloat(_frameLayout.getChildAt(j), View.ROTATION, 0, 90).setDuration(300).start();
                    }
                }

                Log.d("QQ", String.valueOf(camLig.getRotation()));

            }
        };
        orientationEventListener.enable();

        topCam = (LinearLayout) findViewById(R.id.topCam);
        botCam = (LinearLayout) findViewById(R.id.botCam);

        _frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (touch){
                    ObjectAnimator.ofFloat(botCam, View.TRANSLATION_Y, 0).setDuration(300).start();
                    ObjectAnimator.ofFloat(topCam, View.TRANSLATION_Y, 0).setDuration(300).start();

                    touch = false;
                }else{
                    ObjectAnimator.ofFloat(botCam, View.TRANSLATION_Y, botCam.getHeight()).setDuration(300).start();
                    ObjectAnimator.ofFloat(topCam, View.TRANSLATION_Y, -topCam.getHeight()).setDuration(300).start();

                    touch = true;
                }
            }
        });


        if (camParams != null){
            //
            camWhi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final List<String> balance = camParams.getSupportedWhiteBalance();
                    final String[] items = new String[balance.size()];

                    for (int i=0; i<balance.size(); i++){
                        items[i] = balance.get(i);
                    }

                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Wybierz balans bieli!");
                    alert.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            camParams.setWhiteBalance(items[which]);
                            camera.setParameters(camParams);

                        }
                    });
                    alert.show();
                }
            });
            //

            camLig.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int tmpMin = camParams.getMinExposureCompensation();
                    int tmpMax = camParams.getMaxExposureCompensation();
                    int tmp;

                    if (tmpMin<0){
                        tmpMin = Math.abs(tmpMin);
                        tmp = tmpMax + tmpMin;
                    }else {
                        tmp = tmpMax + tmpMin;
                    }

                    final String[] items = new String[tmp+1];
                    for (int i=0; i<=tmp; i++){
                        items[i] = String.valueOf(camParams.getMaxExposureCompensation() - i);
                    }

                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Wybierz naświetlenie!");
                    alert.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            camParams.setExposureCompensation(Integer.parseInt(items[which]));
                            camera.setParameters(camParams);

                        }
                    });
                    alert.show();
                }
            });

            //
            camCol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final List<String> effects = camParams.getSupportedColorEffects();
                    final String[] items = new String[effects.size()];

                    for (int i=0; i<effects.size(); i++){
                        items[i] = effects.get(i);
                    }

                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Wybierz efekt kolorystyczny!");
                    alert.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            camParams.setColorEffect(items[which]);
                            camera.setParameters(camParams);

                        }
                    });
                    alert.show();

                }
            });

            //
            camSiz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final List<Camera.Size> sizes = camParams.getSupportedPictureSizes();
                    final String[] items = new String[sizes.size()];

                    Camera.Size result = null;
                    for (int i=0;i<sizes.size();i++){
                        result = (Camera.Size) sizes.get(i);
                        String tmp = result.width + " x " + result.height;
                        items[i] = tmp;
                    }

                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Wybierz rozmiar zdjęcia!");
                    alert.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Camera.Size result = null;
                            result = (Camera.Size) sizes.get(which);
                            camParams.setPictureSize(result.width , result.height);
                            camera.setParameters(camParams);

                        }
                    });
                    alert.show();

                }
            });
            //
        }

        makePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(null, null, camPictureCallback);
            }
        });

    }

    private void savePicture(){
        //FileOutputStream fs = new FileOutputStream("scieżka do zapisu zdjęcia");
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            fdata = data;
            camera.startPreview();
            imgList.add(data);

            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            Bitmap smallBmp = Bitmap.createScaledBitmap(bitmap, 120, 80, false);

            final Miniatura miniImg = new Miniatura(CameraActivity.this, smallBmp);
            miniImg.setImgID(imgIndex);
            miniImg.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent motionEvent) {

                    Log.d("zz", "pos x: " + motionEvent.getRawX());
                    Log.d("zz", "pos y: " + motionEvent.getRawY());
                    switch(motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x1 = motionEvent.getRawX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = motionEvent.getRawX();
                            float deltaX = x2 - x1;

                            if (Math.abs(deltaX) > 100) {

                                if (x2 > x1) {
                                    // Left to Right swipe
                                    _frameLayout.removeViewAt(2 + miniImg.getImgID());

                                    int tmpId = miniImg.getImgID();
                                    imgList.remove(miniImg.getImgID());

                                    for (int z = tmpId; z<minList.size(); z++){
                                        minList.get(z).setImgID(z);
                                    }
                                    imgIndex = imgIndex - 1;


                                    int tmpK=0;
                                    for(int i=2; i<_frameLayout.getChildCount(); i++) {
                                        tmpAll = 360 / (_frameLayout.getChildCount()-2);

                                        double cos = Math.cos(tmpK * Math.PI / 180);
                                        double sin = Math.sin(tmpK * Math.PI / 180);
                                        float okCos = (float) cos;
                                        float okSin = (float) sin;

                                        _frameLayout.getChildAt(i).setX(((size.x - 700)/2 - 60) + okCos * 350 + 350);
                                        _frameLayout.getChildAt(i).setY((size.y/2) + okSin * 350 - 40);
                                        tmpK += tmpAll;
                                    }

                                } else {
                                    // Right to left swipe
                                }
                            }
                            break;
                        case MotionEvent.ACTION_MOVE:
                            break;
                    }

                    return false;


                }
            });
            miniImg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {


                    Bitmap imgBit = BitmapFactory.decodeByteArray(imgList.get(miniImg.getImgID()), 0, imgList.get(miniImg.getImgID()).length);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    final Bitmap rotatedBitmap = Bitmap.createBitmap(imgBit, 0, 0, imgBit.getWidth(), imgBit.getHeight(), matrix, true);


                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Opcje");
                    final String[] opcje = {"Podgląd","Zapisz bieżące","Usuń bieżące",};

                    alert.setItems(opcje, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (opcje[which]){
                                case "Podgląd":

                                    Log.d("OOO", String.valueOf(miniImg.getImgID()));
                                    Lay.setVisibility(View.VISIBLE);
                                    imgLay.setImageBitmap(rotatedBitmap);

                                    Lay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Lay.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    break;

                                case "Zapisz bieżące":
                                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                                    alert.setTitle("Wybierz lokalizację zdjęcia!");

                                    File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                                    File dir = new File(pic, "Album");
                                    final File[] files = dir.listFiles();

                                    String[] array = new String[files.length];
                                    for(int q=0; q<files.length; q++){
                                        array[q] = files[q].getName();
                                    }

                                    alert.setItems(array, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Log.d("OPT", files[which].getPath());


                                            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                            String d = dFormat.format(new Date());
                                            FileOutputStream fs = null;

                                            try {
                                                fs = new FileOutputStream(files[which].getPath() + "/" + d);
                                                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fs);
                                            } catch (FileNotFoundException e) {

                                            }
                                            try {
                                                fs.close();
                                            } catch (IOException e) {

                                            }
                                        }
                                    });
                                    alert.show();
                                    break;

                                case "Usuń bieżące":
                                    _frameLayout.removeViewAt(2 + miniImg.getImgID());

                                    int tmpId = miniImg.getImgID();
                                    imgList.remove(miniImg.getImgID());

                                    for (int z = tmpId; z<minList.size(); z++){
                                        minList.get(z).setImgID(z);
                                    }
                                    imgIndex = imgIndex - 1;

                                    int tmpK=0;
                                    for(int i=2; i<_frameLayout.getChildCount(); i++) {
                                        tmpAll = 360 / (_frameLayout.getChildCount()-2);

                                        double cos = Math.cos(tmpK * Math.PI / 180);
                                        double sin = Math.sin(tmpK * Math.PI / 180);
                                        float okCos = (float) cos;
                                        float okSin = (float) sin;

                                        _frameLayout.getChildAt(i).setX(((size.x - 700)/2 - 60) + okCos * 350 + 350);
                                        _frameLayout.getChildAt(i).setY((size.y/2) + okSin * 350 - 40);
                                        tmpK += tmpAll;
                                    }
                                    break;
                            }

                        }
                    });

                    alert.show();
                    return false;
                }

            });
            minList.add(miniImg);
            _frameLayout.addView(miniImg);

            /*
            miniImg.setX((size.x - 700)/2 - 60);
            miniImg.setY(size.y/2);
            */
            tmpK=0;
            for(int i=2; i<_frameLayout.getChildCount(); i++) {
                tmpAll = 360 / (_frameLayout.getChildCount()-2);

                double cos = Math.cos(tmpK * Math.PI / 180);
                double sin = Math.sin(tmpK * Math.PI / 180);
                float okCos = (float) cos;
                float okSin = (float) sin;

                _frameLayout.getChildAt(i).setX(((size.x - 700)/2 - 60) + okCos * 350 + 350);
                _frameLayout.getChildAt(i).setY((size.y/2) + okSin * 350 - 40);
                tmpK += tmpAll;
            }
            imgIndex ++;
        }
    };

    @Override
    public void onPause() {
        super.onPause();

        if (camera != null) {
            camera.stopPreview();
            //linijka nieudokumentowana w API, bez niej jest crash przy wznawiamiu kamery
            _cameraPreview.getHolder().removeCallback(_cameraPreview);
            camera.release();
            camera = null;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null) {
            initCamera();
            initPreview();
        }
    }

    public CameraActivity() {
    }

    private int getCameraId(){
        int cid = 0;
        int camerasCount = Camera.getNumberOfCameras(); // gdy więcej niż jedna kamera

        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cid = i;
            }
             /*
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
               cid = i;
            }
            */
        }

        return cid;
    }

    private void initCamera(){

        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if (!cam) {
            // uwaga - brak kamery
        } else {
            // wykorzystanie danych zwróconych przez kolejną funkcję getCameraId()
            cameraId = getCameraId();
            // jest jakaś kamera!
            if (cameraId < 0) {
                // brak kamery z przodu!
            } else {
                camera = Camera.open(cameraId);
            }

        }
    }

    private void initPreview(){
        _cameraPreview = new CameraPreview(CameraActivity.this, camera);
        _frameLayout.addView(_cameraPreview);

        //Kolo
        Kolo aa = new Kolo(CameraActivity.this, size.x, size.y);
        _frameLayout.addView(aa);
        //
    }

    /*
        savePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                alert.setTitle("Wybierz lokalizację zdjęcia!");
                //nie może mieć setMessage!!!

                File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                File dir = new File(pic, "Album");
                final File[] files = dir.listFiles();

                String[] array = new String[files.length];
                for(int q=0; q<files.length; q++){
                    array[q] = files[q].getName();
                }

                alert.setItems(array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Log.d("OPT", files[which].getPath());
                        if (fdata != null){
                            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                            String d = dFormat.format(new Date());

                            FileOutputStream fs = null;
                            try {
                                fs = new FileOutputStream(files[which].getPath() + "/" + d);
                            } catch (FileNotFoundException e) {

                            }
                            try {
                                fs.write(fdata);
                                fs.close();
                            } catch (IOException e) {

                            }

                        }
                    }
                });
                alert.show();

            }
        });
        */
}
