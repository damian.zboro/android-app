package com.example.a4id2.enddz.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.a4id2.enddz.Helpers.ImageData;
import com.example.a4id2.enddz.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChooseCollageActivity extends AppCompatActivity {

    private ImageButton saveCollage;
    private FrameLayout collageFrame;
    private ArrayList<ImageData> lista;
    private Bitmap imgBitmap;
    private ImageView accImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_collage);
        getSupportActionBar().hide();

        imgBitmap = null;
        accImage = null;

        lista = (ArrayList<ImageData>) getIntent().getSerializableExtra("list");
        collageFrame = (FrameLayout) findViewById(R.id.collageFrame);
        collageFrame.setDrawingCacheEnabled(true);


        for (int i=0; i<lista.size(); i++){
            final ImageView iv = new ImageView(ChooseCollageActivity.this);
            iv.setLayoutParams(new FrameLayout.LayoutParams((int) lista.get(i).getW(), (int) lista.get(i).getH()));
            iv.setImageResource(R.drawable.photoadd);
            iv.setBackgroundColor(0xFFFFFFFF);
            iv.setY(lista.get(i).getY());
            iv.setX(lista.get(i).getX());

            iv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(ChooseCollageActivity.this);
                    alert.setTitle("Wybierz skąd pobrać zdjęcie");
                    final String[] opcje = {"Galeria","Aparat"};
                    alert.setItems(opcje, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (opcje[which]){
                                case "Galeria":
                                    Intent intent = new Intent(Intent.ACTION_PICK);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, 100);

                                    accImage = iv;

                                    break;
                                case "Aparat":
                                    Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    if (intentCamera.resolveActivity(getPackageManager()) != null) {
                                        startActivityForResult(intentCamera, 200);

                                        accImage = iv;
                                    }
                                    break;
                            }

                        }
                    });
//
                    alert.show();

                    return false;
                }
            });
            collageFrame.addView(iv);
        }

        saveCollage = (ImageButton) findViewById(R.id.saveCollage);
        saveCollage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(ChooseCollageActivity.this);
                alert.setTitle("Wybierz lokalizację kolażu!");

                File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                File dir = new File(pic, "Album");
                final File[] files = dir.listFiles();

                String[] array = new String[files.length];
                for(int q=0; q<files.length; q++){
                    array[q] = files[q].getName();
                }
                alert.setItems(array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Bitmap collageBitmap = collageFrame.getDrawingCache(true);

                        SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String d = dFormat.format(new Date());
                        FileOutputStream fs = null;

                        try {
                            fs = new FileOutputStream(files[which].getPath() + "/" + d);
                            collageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fs);
                        } catch (FileNotFoundException e) {

                        }
                        try {
                            fs.close();
                        } catch (IOException e) {

                        }

                    }
                });

                alert.show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100){
            Uri imgData = data.getData();
            InputStream stream = null;
            try {
                stream = getContentResolver().openInputStream(imgData);
            } catch (FileNotFoundException e) {

            }
            imgBitmap = BitmapFactory.decodeStream(stream);
        }else if (requestCode == 200){
            Bundle extras = data.getExtras();
            imgBitmap = (Bitmap) extras.get("data");
        }

        if (accImage != null && imgBitmap != null){
            accImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            accImage.setImageBitmap(imgBitmap);
            accImage = null;
            imgBitmap = null;
        }

    }
}
