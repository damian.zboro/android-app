package com.example.a4id2.enddz.Activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.LocaleDisplayNames;
import android.service.quicksettings.Tile;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a4id2.enddz.Adapters.DatabaseManager;
import com.example.a4id2.enddz.Adapters.MyArrayAdapter;
import com.example.a4id2.enddz.Helpers.CustomImageView;
import com.example.a4id2.enddz.Helpers.Note;
import com.example.a4id2.enddz.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class NoteActivity extends AppCompatActivity {

    private ListView list_view;
    private DatabaseManager db;
    private ArrayList <Note> _list;
    private MyArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        list_view = (ListView) findViewById(R.id.list_view);

        db = new DatabaseManager(
                NoteActivity.this,
                "DamianZborowski.db",
                null,
                1
        );
        _list = db.getAll();

        adapter = new MyArrayAdapter(
                NoteActivity.this,
                R.layout.note_layout,
                db.getAll()
        );
        list_view.setAdapter(adapter);

        list_view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final String tmpID = db.getAll().get(position).get_id();
                final String tmpTitle = db.getAll().get(position).getnTitle();
                final String tmpContent = db.getAll().get(position).getnContent();

                AlertDialog.Builder alert = new AlertDialog.Builder(NoteActivity.this);
                alert.setTitle("Options");
                final String[] opcje = {"Edit","Delete","Sort by title", "Sort by content", "Sort by ID"};
                alert.setItems(opcje, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (opcje[which]){
                            case "Delete":
                                db.delete(tmpID);
                                updateList(list_view);
                                break;
                            case "Edit":
                                Intent intent = new Intent(NoteActivity.this,EditionActivity.class);
                                String send = tmpID + "&" + tmpTitle + "&" + tmpContent;
                                intent.putExtra("key", send);
                                startActivity(intent);
                                break;
                            case "Sort by title":

                                Collections.sort(_list, new Comparator<Note>() {
                                    @Override
                                    public int compare(Note a, Note b) {
                                        return a.getnTitle().compareTo(b.getnTitle());
                                    }
                                });
                                adapter = new MyArrayAdapter(
                                        NoteActivity.this,
                                        R.layout.note_layout,
                                        _list
                                );
                                adapter.notifyDataSetChanged();
                                list_view.setAdapter(adapter);

                                break;
                            case "Sort by content":

                                Collections.sort(_list, new Comparator<Note>() {
                                    @Override
                                    public int compare(Note a, Note b) {
                                        return a.getnContent().compareTo(b.getnContent());
                                    }
                                });
                                adapter = new MyArrayAdapter(
                                        NoteActivity.this,
                                        R.layout.note_layout,
                                        _list
                                );
                                adapter.notifyDataSetChanged();
                                list_view.setAdapter(adapter);

                                break;
                            case "Sort by ID":
                                Collections.sort(_list, new Comparator<Note>() {
                                    @Override
                                    public int compare(Note a, Note b) {
                                        return a.get_id().compareTo(b.get_id());
                                    }
                                });
                                adapter = new MyArrayAdapter(
                                        NoteActivity.this,
                                        R.layout.note_layout,
                                        _list
                                );
                                adapter.notifyDataSetChanged();
                                list_view.setAdapter(adapter);
                                break;



                        }

                    }
                });
                alert.show();
                return false;
            }
        });

    }

    private void updateList(ListView list_view){

        MyArrayAdapter adapter = new MyArrayAdapter(
                NoteActivity.this,
                R.layout.note_layout,
                db.getAll()
        );
        adapter.notifyDataSetChanged();
        list_view.setAdapter(adapter);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateList(list_view);
    }

}
