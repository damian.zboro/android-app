package com.example.a4id2.enddz.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.a4id2.enddz.Adapters.MyDrawerAdapter;
import com.example.a4id2.enddz.Helpers.Networking;
import com.example.a4id2.enddz.Helpers.Note;
import com.example.a4id2.enddz.Helpers.UploadFoto;
import com.example.a4id2.enddz.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SingleImg extends AppCompatActivity {

    private ImageView singleImg;
    private ImageButton removeImg;
    private ListView fontList;
    private ArrayList<String> list;
    private MyDrawerAdapter adapter;
    private File myFile;
    private Bitmap myBitmap;
    private ProgressDialog pDialog;

    private byte[] fdata = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_img);

        Bundle bundle = getIntent().getExtras();
        String value = bundle.getString("key");

        pDialog = new ProgressDialog(SingleImg.this);
        pDialog.setMessage("Wysyłanie zdjęcia na serwer!");
        pDialog.setCancelable(false);

        singleImg = (ImageView) findViewById(R.id.singleImg);
        removeImg = (ImageButton) findViewById(R.id.removeImg);

        myFile = new File(value);

        myBitmap = BitmapFactory.decodeFile(myFile.getAbsolutePath());
        singleImg.setImageBitmap(myBitmap);
        singleImg.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        fdata = stream.toByteArray();

        removeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(SingleImg.this);
                alert.setTitle("Co napewno chcesz usunać ten plik?");
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        myFile.delete();
                        finish();
                    }

                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();

            }
        });



        singleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        list = new ArrayList<>();
        list.add("Fonts");
        list.add("Upload");
        list.add("Share");

        adapter = new MyDrawerAdapter(SingleImg.this, R.layout.font_row, list);

        fontList = (ListView) findViewById(R.id.fontList);
        fontList.setAdapter(adapter);

        fontList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (list.get(position)){

                    case "Fonts":
                        Intent intent = new Intent(SingleImg.this, LettersActivity.class);
                        startActivity(intent);
                        break;

                    case "Upload":
                        Networking netAcc = new Networking(SingleImg.this);
                        boolean tmp = netAcc.netAccess();
                        if (tmp){
                            //String tmpUrl = "http://4ia1.spec.pl.hostingasp.pl/test_uploadu/SaveCollage.aspx";
                            String tmpUrl = "http://51.255.196.155/android/save.php";
                            UploadFoto sendFoto =  new UploadFoto(SingleImg.this, pDialog, tmpUrl, fdata);
                            sendFoto.execute();

                        }else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(SingleImg.this);
                            alert.setTitle("Nie udało się połączyć z siecią!");
                            alert.setCancelable(false);
                            alert.setNeutralButton("OK", null).show();
                        }
                        break;

                    case "Share":
                        SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String d = dFormat.format(new Date());

                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/jpeg");
                        String tempFileName = "tymczasowy" + d + ".jpg";

                        File pic = Environment.getExternalStorageDirectory();
                        String tmpPath = pic.getPath();

                        FileOutputStream fs = null;
                        try {
                            fs = new FileOutputStream(tmpPath + "/"+ tempFileName);
                        } catch (FileNotFoundException e) {

                        }
                        try {
                            fs.write(fdata);
                            fs.close();
                        } catch (IOException e) {

                        }
                        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/" + tempFileName)); //pobierz plik i podziel się nim:
                        startActivity(Intent.createChooser(share, "Podziel się plikiem!")); //pokazanie okna share
                        break;

                }

            }
        });



    }
}
