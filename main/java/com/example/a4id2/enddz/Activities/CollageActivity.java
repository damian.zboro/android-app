package com.example.a4id2.enddz.Activities;

import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.example.a4id2.enddz.Helpers.ImageData;
import com.example.a4id2.enddz.R;

import java.util.ArrayList;

public class CollageActivity extends AppCompatActivity {

    private ArrayList<ImageData> list = new ArrayList<>();

    private ImageView collage1;
    private ImageView collage2;
    private ImageView collage3;

    private Display display;
    private Point size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        String value = bundle.getString("key");

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);


        collage1 = (ImageView) findViewById(R.id.collage1);
        collage2 = (ImageView) findViewById(R.id.collage2);
        collage3 = (ImageView) findViewById(R.id.collage3);

        collage1.setOnClickListener(funkcja);
        collage2.setOnClickListener(funkcja);
        collage3.setOnClickListener(funkcja);


    }
    // x y width height
    private View.OnClickListener funkcja = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CollageActivity.this, ChooseCollageActivity.class);
            switch (v.getId()) {
                case R.id.collage1:
                    list.add(new ImageData(0, 0, size.x, size.y/2 - 2));
                    list.add(new ImageData(0, size.y/2 + 4, size.x, size.y/2 - 2));

                    intent.putExtra("list", list);
                    break;
                case R.id.collage2:
                    list.add(new ImageData(0, 0, size.x/2 - 2, (size.y - size.y/3) - 2));
                    list.add(new ImageData(size.x/2 + 4, 0, size.x/2 - 2, (size.y - size.y/3) - 2));
                    list.add(new ImageData(0, (size.y - size.y/3) + 4, size.x, size.y/3 - 2));

                    intent.putExtra("list", list);
                    break;
                case R.id.collage3:
                    list.add(new ImageData(0, 0, size.x/2 - 4, size.y));
                    list.add(new ImageData(size.x/2 + 2, 0, size.x/2 -2, size.y/3 - 2));
                    list.add(new ImageData(size.x/2 + 2, size.y/3 + 4, size.x/2 -2, size.y/3 - 2));
                    list.add(new ImageData(size.x/2 + 2, 2*(size.y/3) + 8, size.x/2 -2, size.y/3 - 2 ));

                    intent.putExtra("list", list);
                    break;
            }
            startActivity(intent);

        }
    };

    @Override
    protected void onRestart() {
        super.onRestart();
        list.clear();
    }

}