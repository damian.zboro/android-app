package com.example.a4id2.enddz.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.a4id2.enddz.Helpers.GetJson;
import com.example.a4id2.enddz.Helpers.Networking;
import com.example.a4id2.enddz.R;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private LinearLayout cameraBt;
    private LinearLayout albumBt;
    private LinearLayout collageBt;
    private LinearLayout networkBt;
    private LinearLayout noteBt;

    private GetJson tmpJSON;
    private ProgressDialog pDialog;

    private Display display;
    private Point size;

    private RelativeLayout slider;


    //ViewPager
    private static ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        slider = (RelativeLayout) findViewById(R.id.slider);
        slider.setLayoutParams(new LinearLayout.LayoutParams(size.x, (int) (size.y * 0.25)));

        mPager = (ViewPager) findViewById(R.id.pager);


        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Pobieranie danych z serwea!");
        pDialog.setCancelable(false);


        Networking netAcc = new Networking(MainActivity.this);
        boolean tmp = netAcc.netAccess();
        if (tmp){
            tmpJSON = new GetJson(MainActivity.this, pDialog, mPager, null, 0, 0);
            tmpJSON.execute();
        }

        // Foldery

        String[] tab1 = new String[]{"Camera","Album","Collage", "Network"};
        String[] tab2 = new String[]{"ludzie","miejsca","rzeczy"};

        File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        for (int i=0; i<tab1.length; i++){
            if(tab1[i]=="Album"){
                File dir = new File(pic, tab1[i]);
                dir.mkdir();

                for (int j=0; j<tab2.length; j++){
                    File dir2 = new File(dir.getPath(), tab2[j]);
                    dir2.mkdir();
                }

            }else {
                File dir = new File(pic, tab1[i]);
                dir.mkdir();
            }

        }


        //

        cameraBt = (LinearLayout) findViewById(R.id.camera);
        cameraBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                String send = "Camera";
                intent.putExtra("key", send);
                startActivity(intent);
            }
        });

        //

        albumBt = (LinearLayout) findViewById(R.id.album);
        albumBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
                startActivity(intent);
            }
        });

        //

        collageBt = (LinearLayout) findViewById(R.id.collage);
        collageBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CollageActivity.class);
                String send = "Collage";
                intent.putExtra("key", send);
                startActivity(intent);
            }
        });

        //

        networkBt = (LinearLayout) findViewById(R.id.network);
        networkBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String[] viewImg = new String []{"LinearLayout", "ViewPager"};
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Wybierz typ podglądu zdjęć!");
                alert.setItems(viewImg, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this,NetworkActivity.class);
                        String send = "";
                        switch (viewImg[which]){
                            case "LinearLayout":
                                send = "LinearLayout";
                                break;
                            case "ViewPager":
                                send = "ViewPager";
                                break;
                        }
                        intent.putExtra("key", send);
                        startActivity(intent);
                    }
                });
                alert.show();

            }
        });


        noteBt = (LinearLayout) findViewById(R.id.note);
        noteBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,NoteActivity.class);
                startActivity(intent);
            }
        });

    }

}
