package com.example.a4id2.enddz.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.a4id2.enddz.Helpers.GetJson;
import com.example.a4id2.enddz.Helpers.Networking;
import com.example.a4id2.enddz.R;

public class NetworkActivity extends AppCompatActivity {

    private LinearLayout networkLayout;
    private GetJson tmpJSON;
    private ProgressDialog pDialog;
    private static ViewPager mPager;

    private Display display;
    private Point size;

    private RelativeLayout netRel;
    private ScrollView imgScroll;
    private LinearLayout imgGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        String value = bundle.getString("key");

        netRel = (RelativeLayout) findViewById(R.id.netRel);
        imgScroll = (ScrollView) findViewById(R.id.imgScroll);
        imgGallery = (LinearLayout) findViewById(R.id.imgGallery);

        networkLayout = (LinearLayout) findViewById(R.id.networkLayout);
        mPager = (ViewPager) findViewById(R.id.netPager);

        pDialog = new ProgressDialog(NetworkActivity.this);
        pDialog.setMessage("Pobieranie danych z serwea!");
        pDialog.setCancelable(false);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        Networking netAcc = new Networking(NetworkActivity.this);
        boolean tmp = netAcc.netAccess();
        if (tmp){

            switch (value){
                case "LinearLayout":
                    netRel.setVisibility(View.GONE);

                    tmpJSON = new GetJson(NetworkActivity.this, pDialog, null, imgGallery, size.x, size.y);
                    tmpJSON.execute();

                    break;

                case "ViewPager":
                    imgScroll.setVisibility(View.GONE);
                    tmpJSON = new GetJson(NetworkActivity.this, pDialog, mPager, null, 0, 0);
                    tmpJSON.execute();

                    break;

            }

        }


    }
}
