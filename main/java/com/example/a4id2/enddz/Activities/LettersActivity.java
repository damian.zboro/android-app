package com.example.a4id2.enddz.Activities;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.a4id2.enddz.Helpers.PreviewText;
import com.example.a4id2.enddz.R;

import java.io.IOException;

public class LettersActivity extends AppCompatActivity {

    private LinearLayout topFont;
    private ScrollView scrollFont;
    private LinearLayout fontType;
    private RelativeLayout textLayout;

    private EditText textBox;

    private Display display;
    private Point size;

    private Typeface tmpTF;
    private PreviewText pText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letters);
        getSupportActionBar().hide();

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        pText = null;

        fontType = (LinearLayout) findViewById(R.id.fontType);
        topFont = (LinearLayout) findViewById(R.id.topFont);
        scrollFont = (ScrollView) findViewById(R.id.scrollFont);

        topFont.setLayoutParams(new RelativeLayout.LayoutParams(size.x, (int) (size.y * 0.3)));
        topFont.setY(0);
        scrollFont.setLayoutParams(new RelativeLayout.LayoutParams(size.x, (int) (size.y * 0.7)));
        scrollFont.setY((float) (size.y * 0.3));

        textBox = (EditText) findViewById(R.id.textBox);

        AssetManager assetManager = getAssets();
        try {

            final String[] lista = assetManager.list("fonts");
            tmpTF = Typeface.createFromAsset(getAssets(),"fonts/" + lista[2]);

            for (int i=0; i<lista.length; i++){

                TextView tv = new TextView(LettersActivity.this);
                Typeface tf= Typeface.createFromAsset(getAssets(),"fonts/" + lista[i]);
                tv.setTypeface (tf);
                tv.setText("zxcv asdf QWER");
                tv.setGravity(Gravity.CENTER);
                tv.setTextSize(40);
                tv.setPadding(15, 15, 15, 15);
                final int finalI = i;
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (pText != null){
                            tmpTF = Typeface.createFromAsset(getAssets(),"fonts/" + lista[finalI]);
                            String tmpStr = pText.getStr();

                            textLayout.removeAllViews();
                            pText = new PreviewText(LettersActivity. this, tmpTF, tmpStr, (float) (size.x * 0.2), (float) (size.y * 0.13));
                            textLayout.addView(pText);
                        }
                    }
                });

                fontType.addView(tv);
            }

        } catch (IOException e) {

        }

        textLayout = (RelativeLayout) findViewById(R.id.textLayout);
        textLayout.setLayoutParams(new LinearLayout.LayoutParams(size.x, (int) (size.y * 0.2)));

        pText = new PreviewText(LettersActivity. this, tmpTF, "zxcv text", (float) (size.x * 0.2), (float) (size.y * 0.13));
        textLayout.addView(pText);


        TextWatcher textWatcher = new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                textLayout.removeAllViews();
                pText = new PreviewText(LettersActivity. this, tmpTF, editable.toString(), (float) (size.x * 0.2), (float) (size.y * 0.13));
                textLayout.addView(pText);
            }
        };

        textBox.addTextChangedListener(textWatcher);

    }
}
