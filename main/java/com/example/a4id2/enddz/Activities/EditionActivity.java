package com.example.a4id2.enddz.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.a4id2.enddz.Adapters.DatabaseManager;
import com.example.a4id2.enddz.R;

public class EditionActivity extends AppCompatActivity {

    private EditText newTitle;
    private EditText newContent;
    private LinearLayout newColors;
    private Button backEdit;
    private Button saveEdit;
    private DatabaseManager db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edition);

        db = new DatabaseManager(
                EditionActivity.this,
                "DamianZborowski.db",
                null,
                1
        );

        Bundle bundle = getIntent().getExtras();
        final String value = bundle.getString("key");
        final String[] parts = value.split("&");

        newTitle = (EditText) findViewById(R.id.newTitle);
        newContent = (EditText) findViewById(R.id.newContent);
        newColors = (LinearLayout) findViewById(R.id.newColors);

        final String[] color = new String[1];
        color[0] = null;

        final String[] tmpTitle = {parts[1]};
        final String[] tmpContent = {parts[2]};

        newTitle.setText(tmpTitle[0]);
        newContent.setText(tmpContent[0]);


        final int[] colors = new int[]{0xFFFF0000, 0xFF00FFFF, 0xFFFF00FF, 0xFF0000FF};
        for (int i=0; i<colors.length; i++){

            LinearLayout newLayout = new LinearLayout(EditionActivity.this);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    newColors.getLayoutParams().width/colors.length,
                    newColors.getLayoutParams().height,
                    1.0f
            );
            newLayout.setLayoutParams(param);
            newLayout.setBackgroundColor(colors[i]);
            newLayout.setOrientation(LinearLayout.VERTICAL);
            final int finalI = i;
            newLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    color[0] = String.valueOf(colors[finalI]);
                }
            });
            newColors.addView(newLayout);

        }

        saveEdit = (Button) findViewById(R.id.saveEdit);
        saveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tmpTitle[0] = newTitle.getText().toString();
                tmpContent[0] = newContent.getText().toString();
                db.update(parts[0], tmpTitle[0], tmpContent[0], color[0]);

                finish();
            }
        });

        backEdit = (Button) findViewById(R.id.backEdit);
        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}
