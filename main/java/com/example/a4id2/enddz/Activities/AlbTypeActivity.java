package com.example.a4id2.enddz.Activities;

import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.example.a4id2.enddz.Adapters.DatabaseManager;
import com.example.a4id2.enddz.Helpers.CustomImageView;
import com.example.a4id2.enddz.R;

import java.io.File;

public class AlbTypeActivity extends AppCompatActivity {

    private ImageButton removeBt;
    private String value;
    private LinearLayout gallery;
    private String imagepath;
    private LinearLayout.LayoutParams lparams;
    private int x;
    private int y;
    private boolean flag = true;
    private ScrollView scrollDiv;
    private int count;
    private boolean tmp;
    private DatabaseManager db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alb_type);

        db = new DatabaseManager(
                AlbTypeActivity.this,
                "DamianZborowski.db",
                null,
                1 //wersja bazy, po zmianie schematu bazy należy ją zwiększyć
        );

        Bundle bundle = getIntent().getExtras();
        value = bundle.getString("key");

        gallery = (LinearLayout) findViewById(R.id.gallery);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        x = size.x;
        y = size.y;


        File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
        File dir = new File(pic, "Album");
        File dir2 = new File(dir, value);

        scrollDiv = (ScrollView) findViewById(R.id.scrollDiv);

        if(dir2.listFiles().length != 0){

            if (dir2.listFiles().length % 2 == 0){
                count = dir2.listFiles().length/2;
                tmp = false;
            }else if(dir2.listFiles().length % 2 == 1){
                count = (dir2.listFiles().length + 1)/2;
                tmp = true;
            }

            for (int i=0; i<count; i++){
                LinearLayout newLayout = new LinearLayout(AlbTypeActivity.this);
                newLayout.setOrientation(LinearLayout.HORIZONTAL);
                newLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, y/6));

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(newLayout.getLayoutParams());
                lp.setMargins(0, 10, 0, 0);
                newLayout.setLayoutParams(lp);


                if(i % 2 == 0){
                    flag = false;
                }else{
                    flag = true;
                }

                if(tmp == false){

                    File newFile = dir2.listFiles()[i*2];
                    if(newFile.isFile()){
                        CustomImageView civ = new CustomImageView(AlbTypeActivity.this, true, flag, newFile, x, y, db);
                        newLayout.addView(civ);
                    }

                    File newFile2 = dir2.listFiles()[(i*2)+1];
                    if(newFile2.isFile()){
                        CustomImageView civ = new CustomImageView(AlbTypeActivity.this, false, flag, newFile2, x, y, db);
                        newLayout.addView(civ);
                    }

                }else{

                    if(i < count - 1){
                        File newFile = dir2.listFiles()[i*2];
                        if(newFile.isFile()){
                            CustomImageView civ = new CustomImageView(AlbTypeActivity.this, true, flag, newFile, x, y, db);
                            newLayout.addView(civ);
                        }

                        File newFile2 = dir2.listFiles()[(i*2)+1];
                        if(newFile2.isFile()){
                            CustomImageView civ = new CustomImageView(AlbTypeActivity.this, false, flag, newFile2, x, y, db);
                            newLayout.addView(civ);
                        }
                    }else {
                        File newFile = dir2.listFiles()[i*2];
                        if(newFile.isFile()){
                            CustomImageView civ = new CustomImageView(AlbTypeActivity.this, true, flag, newFile, x, y, db);
                            newLayout.addView(civ);
                        }
                    }


                }

                gallery.addView(newLayout);

            }
        }


        //
        removeBt = (ImageButton) findViewById(R.id.removeBt);
        removeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(AlbTypeActivity.this);
                alert.setTitle("Czy na pewno chcesz usunąć ten folder?");
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                        File dir = new File(pic, "Album");
                        File dir2 = new File(dir, value);

                        for (File file : dir2.listFiles()){
                            file.delete();
                        }

                        dir2.delete();
                        finish();
                    }

                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();

            }
        });
        //


    }


    @Override
    protected void onRestart() {
        super.onRestart();

    }
}
