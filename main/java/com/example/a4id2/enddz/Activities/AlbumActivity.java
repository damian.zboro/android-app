package com.example.a4id2.enddz.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.a4id2.enddz.R;

import java.io.File;

public class AlbumActivity extends AppCompatActivity {

    private ListView lvAlb;
    private ImageButton removeBt;
    private ImageButton addBt;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        lvAlb = (ListView) findViewById(R.id.lvAlb);
        //removeBt = (ImageButton) findViewById(R.id.removeBt);
        addBt = (ImageButton) findViewById(R.id.addBt);


        addBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(AlbumActivity.this);
                alert.setTitle("Podaj nazwę folderu!");
                input = new EditText(AlbumActivity.this);
                input.setText("nowy");
                alert.setView(input);
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
                        File dir = new File(pic, "Album");

                        String val = input.getText().toString();
                        File dir2 = new File(dir.getPath(), val);
                        dir2.mkdir();

                        File[] files = dir.listFiles();

                        final String[] array = new String[files.length];
                        for(int q=0; q<files.length; q++){
                            array[q] = files[q].getName();
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                AlbumActivity.this,     // Context
                                R.layout.row_layout,     // nazwa pliku xml naszego wiersza
                                R.id.row,         // id pola txt w wierszu
                                array );         // tablica przechowująca dane

                        lvAlb.setAdapter(adapter);

                        lvAlb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                //test
                                Log.d("TAG","index = " + array[i]);

                                Intent intent = new Intent(AlbumActivity.this, AlbTypeActivity.class);
                                String msg = array[i];

                                intent.putExtra("key", msg);
                                startActivity(intent);

                            }
                        });

                    }

                });


                alert.show();
            }
        });

        //
        File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
        File dir = new File(pic, "Album");
        File[] files = dir.listFiles();

        final String[] array = new String[files.length];
        for(int q=0; q<files.length; q++){
            array[q] = files[q].getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                AlbumActivity.this,     // Context
                R.layout.row_layout,     // nazwa pliku xml naszego wiersza
                R.id.row,         // id pola txt w wierszu
                array );         // tablica przechowująca dane

        lvAlb.setAdapter(adapter);

        //

        lvAlb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //test
                Log.d("TAG","index = " + array[i]);

                Intent intent = new Intent(AlbumActivity.this,AlbTypeActivity.class);
                String msg = array[i];

                intent.putExtra("key", msg);
                startActivity(intent);

            }
        });

        //



    }

    @Override
    protected void onRestart() {
        super.onRestart();

        //
        File pic = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );
        File dir = new File(pic, "Album");
        File[] files = dir.listFiles();

        final String[] array = new String[files.length];
        for(int q=0; q<files.length; q++){
            array[q] = files[q].getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                AlbumActivity.this,     // Context
                R.layout.row_layout,     // nazwa pliku xml naszego wiersza
                R.id.row,         // id pola txt w wierszu
                array );         // tablica przechowująca dane

        lvAlb.setAdapter(adapter);

        //

        lvAlb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //test
                Log.d("TAG","index = " + array[i]);

                Intent intent = new Intent(AlbumActivity.this,AlbTypeActivity.class);
                String msg = array[i];

                intent.putExtra("key", msg);
                startActivity(intent);

            }
        });

        //
    }
}
