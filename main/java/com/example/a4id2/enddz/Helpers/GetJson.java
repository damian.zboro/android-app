package com.example.a4id2.enddz.Helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.example.a4id2.enddz.Activities.MainActivity;
import com.example.a4id2.enddz.Adapters.SlidingImageAdapter;
import com.example.a4id2.enddz.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by 4id2 on 2017-12-13.
 */
public class GetJson extends AsyncTask <String, Void, String>{

    private Context context;
    private ProgressDialog pDialog;
    private JSONArray allImagesJson = null;
    private ArrayList<ImageDataJSON> listaJSON = new ArrayList<>();

    private JSONObject object;

    //ViewPager

    private ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    //

    private LinearLayout lay;
    private int width;
    private int height;

    public GetJson(Context context, ProgressDialog pDialog, ViewPager mPager, LinearLayout lay, int width, int height){
        this.context = context;
        this.pDialog = pDialog;
        this.mPager = mPager;
        this.lay = lay;
        this.width = width;
        this.height = height;
    }

    @Override
    protected String doInBackground(String... params) {
        HttpPost httpPost = new HttpPost("http://51.255.196.155/android/getjson.php");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonString = null;
        try {
            jsonString = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //jesli jsonString nie jest pusty wtedy parsujemy go na obiekt JSON
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //a potem rozbijamy na tablicę obiektów
        try {
            allImagesJson = jsonObj.getJSONArray("ImagesList");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //teraz mogę pobierać dane for-em z elementów tej tablicy
        for (int i = 0; i < allImagesJson.length(); i++) {
            // obiekty po kolei
            try {
                object = allImagesJson.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String imageName = null;
            String imageSaveTime = null;
            String imageSize = null;
            try {
                imageName = object.getString("imageName");
                String[] tmp = imageName.split("_");

                if (tmp.length == 3){
                    imageSaveTime = object.getString("imageSaveTime");
                    imageSize = object.getString("imageSize");
                    listaJSON.add(new ImageDataJSON(imageName, imageSaveTime, imageSize));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        pDialog.dismiss();

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Dane pobrane");
        alert.setCancelable(false);
        alert.setNeutralButton("OK", null).show();

        if (mPager == null ){

            for (int i=0; i<listaJSON.size(); i++){

                ImageView iv = new ImageView(context);
                iv.setLayoutParams(new LinearLayout.LayoutParams(width, height/2));
                new LoadImageTask(iv).execute("http://51.255.196.155/android/img/" + listaJSON.get(i).getImageName());
                final int finalI = i;
                iv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        String[] tmp = listaJSON.get(finalI).getImageName().split("_");
                        String img = tmp[0] + "_" + tmp[1] + ".jpeg";
                        Intent intent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://51.255.196.155/android/img/" + img)
                        );
                        context.startActivity(intent);
                        return true;
                    }
                });
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                lay.addView(iv);
            }

        }else {
            init();
        }


    }


    private void init() {

        mPager.setAdapter(new SlidingImageAdapter(context, listaJSON));
        NUM_PAGES = listaJSON.size();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

    }


}
