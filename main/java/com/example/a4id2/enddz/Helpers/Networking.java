package com.example.a4id2.enddz.Helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by 4id2 on 2017-12-06.
 */
public class Networking {

    private Context context;

    public Networking(Context context) {
        this.context = context;
    }

    public boolean netAccess(){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }else {
            return true;
        }

    }
}