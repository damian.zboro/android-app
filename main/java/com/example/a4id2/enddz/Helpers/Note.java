package com.example.a4id2.enddz.Helpers;

public class Note {
    private String nTitle;
    private String nContent;
    private String nColor;
    private String nPath;
    private String _id;


    public Note(String _id, String nTitle, String nContent, String nColor, String nPath) {
        this._id = _id;
        this.nTitle = nTitle;
        this.nContent = nContent;
        this.nColor = nColor;
        this.nPath = nPath;

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getnTitle() {
        return nTitle;
    }

    public void setnTitle(String nTitle) {
        this.nTitle = nTitle;
    }

    public String getnContent() {
        return nContent;
    }

    public void setnContent(String nContent) {
        this.nContent = nContent;
    }

    public String getnColor() {
        return nColor;
    }

    public void setnColor(String nColor) {
        this.nColor = nColor;
    }

    public String getnPath() {
        return nPath;
    }

    public void setnPath(String nPath) {
        this.nPath = nPath;
    }
}