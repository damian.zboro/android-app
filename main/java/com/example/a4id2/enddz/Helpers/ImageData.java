package com.example.a4id2.enddz.Helpers;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by 4id2 on 2017-11-08.
 */
public class ImageData implements Serializable{
    private float x;
    private float y;
    private float w;
    private float h;

    public ImageData(float x, float y, float w, float h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getW() {
        return w;
    }

    public float getH() {
        return h;
    }
}
