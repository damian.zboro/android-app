package com.example.a4id2.enddz.Helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import com.android.internal.http.multipart.MultipartEntity;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by 4id2 on 2017-12-06.
 */
public class UploadFoto extends AsyncTask<String, Void, String>{
    private Context context;
    private ProgressDialog pDialog;
    private String urlServer;
    private byte[] data;
    private String result;

    public UploadFoto(Context context, ProgressDialog pDialog, String urlServer, byte[] data) {
        this.context = context;
        this.pDialog = pDialog;
        this.urlServer = urlServer;
        this.data = data;
    }


    @Override
    protected String doInBackground(String... params) {
        HttpPost httpPost = new HttpPost(urlServer);

        String imageEncoded = Base64.encodeToString(data, Base64.DEFAULT);

        //httpPost.setEntity(new ByteArrayEntity(data));
        try {
            httpPost.setEntity(new StringEntity(imageEncoded, "UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpPost);
            result = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8);
            Log.d("ZXC", result);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        pDialog.dismiss();


        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(result);
        alert.setCancelable(false);
        alert.setNeutralButton("OK", null).show();


    }
}
