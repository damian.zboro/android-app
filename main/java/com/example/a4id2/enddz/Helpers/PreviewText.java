package com.example.a4id2.enddz.Helpers;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;

public class PreviewText extends View {
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Typeface tf;
    private String str;
    private float x;
    private float y;

    public String getStr() {
        return str;
    }

    public PreviewText(Context context, Typeface tf, String str, float x, float y) {
        super(context);
        this.tf = tf;
        this.str = str;
        this.x = x;
        this.y = y;


        paint.reset();
        paint.setAntiAlias(true);
        paint.setTextSize(200);
        paint.setTypeface(this.tf);
        this.setBackgroundColor(Color.BLACK);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        canvas.drawText(str,  x,  y, paint);

    }
}