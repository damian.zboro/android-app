package com.example.a4id2.enddz.Helpers;

import android.graphics.drawable.Drawable;
import android.icu.text.LocaleDisplayNames;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by 4id2 on 2017-12-13.
 */
public class LoadImageTask extends AsyncTask<String, Void, String> {

    private Drawable loadedImage;
    private ImageView iv;

    public LoadImageTask(ImageView iv) {
        this.iv = iv;
    }

    @Override
    protected String doInBackground(String... params) {
        String imgUrl = params[0];
        loadedImage = LoadImageFromWeb(imgUrl);

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        iv.setImageDrawable(loadedImage);
    }

    public Drawable LoadImageFromWeb(String url) {

        InputStream inputStream = null;
        try {
            inputStream = (InputStream) new URL(url).getContent();
        } catch (IOException e) {

        }
        return Drawable.createFromStream(inputStream, "src name");

    }
}
