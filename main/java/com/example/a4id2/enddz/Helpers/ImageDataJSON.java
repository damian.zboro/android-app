package com.example.a4id2.enddz.Helpers;

/**
 * Created by 4id2 on 2017-12-13.
 */
public class ImageDataJSON {

    private String imageName;
    private String imageSaveTime;
    private String imageSize;

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public ImageDataJSON(String imageName, String imageSaveTime, String imageSize) {
        this.imageName = imageName;
        this.imageSaveTime = imageSaveTime;
        this.imageSize = imageSize;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageSaveTime() {
        return imageSaveTime;
    }

    public void setImageSaveTime(String imageSaveTime) {
        this.imageSaveTime = imageSaveTime;
    }
}
