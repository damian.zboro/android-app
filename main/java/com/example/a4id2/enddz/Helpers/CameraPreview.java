package com.example.a4id2.enddz.Helpers;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.hardware.Camera;

import java.io.IOException;

/**
 * Created by 4id2 on 2017-09-27.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private Camera _camera;
    private SurfaceHolder _surfaccHolder;

    public CameraPreview(Context context, Camera camera) {
        super(context);

        this._camera = camera;
        this._surfaccHolder = this.getHolder();
        this._surfaccHolder.addCallback(this);
        this._camera.setDisplayOrientation(90);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            _camera.setPreviewDisplay(_surfaccHolder);
            _camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            _camera.setPreviewDisplay(_surfaccHolder);
            _camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
