package com.example.a4id2.enddz.Helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.example.a4id2.enddz.Activities.SingleImg;
import com.example.a4id2.enddz.Adapters.DatabaseManager;
import com.example.a4id2.enddz.R;

import java.io.File;

public class CustomImageView extends ImageView implements View.OnClickListener, View.OnLongClickListener{

    private int x;
    private int y;
    private File file;
    private boolean type;
    private boolean flag;
    private Context context;
    private DatabaseManager db;

    public CustomImageView(Context context, boolean type, boolean flag, File file, int x, int y, DatabaseManager db) {
        super(context);

        this.file = file;
        this.context = context;
        this.db = db;


        ImageView imageview = new ImageView(context);
        this.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams lparams;

        if (type){
            if (flag){
                lparams = new LinearLayout.LayoutParams(x/3, y/6);
            } else {
                lparams = new LinearLayout.LayoutParams(x-x/3, y/6);
            }
        }else{
            if (flag){
                lparams = new LinearLayout.LayoutParams(x-x/3, y/6);
            } else {
                lparams = new LinearLayout.LayoutParams(x/3, y/6);
            }
        }

        this.setLayoutParams(lparams);
        String imagepath = file.getPath();
        Bitmap bmp = betterImageDecode(imagepath);
        this.setImageBitmap(bmp);

        setOnClickListener(this);
        setOnLongClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String str = file.getPath();

        Intent intent = new Intent(context,SingleImg.class);
        intent.putExtra("key", str);
        context.startActivity(intent);
    }

    @Override
    public boolean onLongClick(View view) {

        view = View.inflate(context, R.layout.coustomdialog, null);

        final EditText etTitle = (EditText) view.findViewById(R.id.etTitle);
        final EditText etContent = (EditText) view.findViewById(R.id.etContent);
        final LinearLayout colorchoose = (LinearLayout) view.findViewById(R.id.colorchoose);

        final String[] color = new String[1];
        color[0] = String.valueOf(0xff000000);


        final int[] colors = new int[]{0xFFFF0000, 0xFF00FFFF, 0xFFFF00FF, 0xFF0000FF};
        for (int i=0; i<colors.length; i++){

            LinearLayout newLayout = new LinearLayout(context);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    colorchoose.getLayoutParams().width/colors.length,
                    colorchoose.getLayoutParams().height,
                    1.0f
            );
            newLayout.setLayoutParams(param);
            newLayout.setBackgroundColor(colors[i]);
            newLayout.setOrientation(LinearLayout.VERTICAL);
            final int finalI = i;
            newLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("color", String.valueOf(colors[finalI]));

                    color[0] = String.valueOf(colors[finalI]);
                }
            });

            colorchoose.addView(newLayout);

        }


        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(view);
        alert.setTitle("Note");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String title = etTitle.getText().toString();
                String content = etContent.getText().toString();
                db.insert(title, content, color[0], file.getPath());
            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();

        return true;
    }


    private Bitmap betterImageDecode(String filePath) {

        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        myBitmap = BitmapFactory.decodeFile(filePath, options);
        return myBitmap;
    }

}