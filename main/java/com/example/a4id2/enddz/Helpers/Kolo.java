package com.example.a4id2.enddz.Helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

/**
 * Created by 4id2 on 2017-10-18.
 */
public class Kolo extends View {
    private int xSize;
    private int ySize;


    public Kolo(Context context, int xSize, int ySize) {
        super(context);
        this.xSize = xSize;
        this.ySize = ySize;

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        //paint.setColor(Color.argb(150, 255, 255, 255));
        paint.setColor(Color.RED);
        canvas.drawCircle(xSize/2, ySize/2, 350, paint);


    }

}
