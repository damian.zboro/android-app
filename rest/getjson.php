<?php
header('Content-Type: application/json');
$tmpJSON = "{\"ImagesList\":[";

$directory = "img/";
$filecount = 0;
$files = glob($directory . "*");
if ($files){
 $filecount = count($files) - 1;
}

$tmpI = 0;

$handle = opendir(dirname(realpath(__FILE__)).'/img/');
while($file = readdir($handle)){
    if($file !== '.' && $file !== '..'){
        $tmp = explode(".", $file);
        $tmpSave = explode("_", $tmp[0]);

        $save = $tmpSave[0]." ".$tmpSave[1];
        $sizeImg = filesize(__DIR__.'/img/'.$file); 

        $tmpJSON .= "{";
        $tmpJSON .= "\"imageName\": \"".$file."\",";
        $tmpJSON .= "\"imageSaveTime\": \"".$save."\",";
        $tmpJSON .= "\"imageSize\": \"".$sizeImg."\"";
        if($tmpI < $filecount){
            $tmpJSON .= "},";
        }else{
            $tmpJSON .= "}";
        }
        
        $tmpI ++;
    }
}

$tmpJSON .= "]}";

echo $tmpJSON;

?>