<?php

$value = file_get_contents('php://input');
if($value == null){
    echo 'Brak danych!';
}else{
    $name = date('Y-m-d_H:m:s');
    $imageName = 'img/'.(string)$name.'.jpeg';
    
    $imageData = base64_decode($value);
    
    $source = imagecreatefromstring($imageData);
    $imageSave = imagejpeg($source,$imageName,100);
    imagedestroy($source);
    
    $iconName = 'img/'.(string)$name.'_icon.jpeg';
    $iconSource = imagecreatefromstring($imageData);
    $width = imagesx($iconSource);
    $height = imagesy($iconSource);
    $img = imagecreatetruecolor(100,100);
    imagecopyresized($img,$iconSource,0,0,0,0,100,100,$width,$height);
    imagejpeg($img, $iconName);
    imagedestroy($img);
    
    
    echo 'Zdjęcie zostło zapisane!';
}

?>
